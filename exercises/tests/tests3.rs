// tests3.rs
// This test isn't testing our function -- make it do that in such a way that
// the test passes. Then write a second test that tests whether we get the result
// we expect to get when we call `is_even(5)`.
// Execute `rustlings hint tests3` for hints :)

pub fn is_even(num: i32) -> bool {
    num % 2 == 0
}

struct Rectagle {
    width: u32,
    height: u32,
}

impl Rectagle {
    fn can_fit(&self, other: &Rectagle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_true_when_even() {
        assert!(is_even(4));
    }

    #[test]
    fn larger_can_fit_smaller() {
        let larger = Rectagle {
            width: 4,
            height: 5,
        };
        let smaller = Rectagle {
            width: 2,
            height: 3,
        };

        assert!(larger.can_fit(&smaller));
    }

    #[test]
    fn smaller_cannot_fit_larger() {
        let larger = Rectagle {
            width: 4,
            height: 5,
        };
        let smaller = Rectagle {
            width: 2,
            height: 3,
        };

        assert!(!smaller.can_fit(&larger));
    }
}
